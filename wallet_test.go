package wallet

import (
	"testing"
)

func TestWallet(t *testing.T) {
	assertBal := func(t testing.TB, wallet Wallet, want Bitcoin) {
		t.Helper()

		got := wallet.Balance()

		if got != want {
			t.Errorf("got %s want %s", got, want)
		}
	}

	assertErr := func(t testing.TB, got error, want error) {
		t.Helper()

		if got == nil {
			t.Fatal("wanted an error but didn't get one")
		}

		if got != want {
			t.Errorf("got %q, want %q", got, want)
		}
	}

	assertNoErr := func(t testing.TB, got error) {
		t.Helper()
		if got != nil {
			t.Fatal("got an error but didn't want one")
		}
	}

	t.Run("Deposit", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(10))
		assertBal(t, wallet, Bitcoin(10))
	})

	t.Run("Withdraw", func(t *testing.T) {
		wallet := Wallet{balance: Bitcoin(20)}
		err := wallet.Withdraw(Bitcoin(10))
		assertBal(t, wallet, Bitcoin(10))
		assertNoErr(t, err)
	})

	t.Run("Overdraw funds", func(t *testing.T) {
		startingBal := Bitcoin(20)
		wallet := Wallet{startingBal}

		err := wallet.Withdraw(Bitcoin(100))

		assertBal(t, wallet, startingBal)
		assertErr(t, err, ErrInsufficientFunds)
	})
}
