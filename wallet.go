package wallet

import (
	"errors"
	"fmt"
)

// Bitcoin is our wallet type, just maps to int
type Bitcoin int

// Stringer interface creates the String() method to define how to print something
// of a given type
type Stringer interface {
	String() string
}

// String defines how to print Bitcoin values
func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

// Wallet is a struct that contains the balance of the wallet
type Wallet struct {
	balance Bitcoin
}

// Deposit adds to the balance for the wallet
func (w *Wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

// Balance returns the balance for the wallet
func (w *Wallet) Balance() Bitcoin {
	// in go struct poBitcoiners are automatically dereferenced, so we don't have to do
	// return (*w).balance
	// beacuse that would be toooo cumbersome
	return w.balance
}

var ErrInsufficientFunds = errors.New("cannot withdraw, account would have been overdrawn")

// Withdraw takes money out of the account. Returns an error if it would result
// in an overdrawn account
func (w *Wallet) Withdraw(amount Bitcoin) error {
	if amount > w.balance {
		return ErrInsufficientFunds
	}

	w.balance -= amount
	return nil
}
